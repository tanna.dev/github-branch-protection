package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/google/go-github/v50/github"
)

func report(ctx context.Context, client *github.Client, organisation, repo, branch string) []string {
	fields := []string{organisation, repo, branch}

	_, _, err := client.Repositories.GetBranch(ctx, organisation, repo, branch, false)
	if err != nil {
		fields = append(fields, "false")
		return fields
	}
	fields = append(fields, "true")

	p, _, err := client.Repositories.GetBranchProtection(ctx, organisation, repo, branch)
	if err != nil {
		fields = append(fields, "false")
		return fields
	}
	fields = append(fields, "true")

	if p.RequiredStatusChecks == nil {
		fields = append(fields, "false")
		fields = append(fields, "")
		fields = append(fields, "false")
	} else {
		fields = append(fields, strconv.FormatBool(len(p.RequiredStatusChecks.Contexts) > 0))
		fields = append(fields, strings.Join(p.RequiredStatusChecks.Contexts, ","))
		fields = append(fields, strconv.FormatBool(p.RequiredStatusChecks.Strict))
	}

	pushRules := []string{}
	if p.Restrictions != nil {
		for _, a := range p.Restrictions.Apps {
			if a.Slug != nil {
				pushRules = append(pushRules, fmt.Sprintf("app/%v", *a.Slug))
			}
		}
		for _, t := range p.Restrictions.Teams {
			if t.Slug != nil {
				pushRules = append(pushRules, fmt.Sprintf("team/%v", *t.Slug))
			}
		}
		for _, u := range p.Restrictions.Users {
			if u.Name != nil {
				pushRules = append(pushRules, fmt.Sprintf("user/%v", *u.Name))
			}
		}
	}
	fields = append(fields, strings.Join(pushRules, ","))

	fields = append(fields, strconv.FormatBool(p.EnforceAdmins.Enabled))

	fields = append(fields, strconv.FormatBool(p.AllowForcePushes.Enabled))
	fields = append(fields, strconv.FormatBool(p.AllowDeletions.Enabled))

	if p.RequiredPullRequestReviews == nil {
		fields = append(fields, "false")
		fields = append(fields, "false")
		fields = append(fields, "")

	} else {
		fields = append(fields, "true")
		fields = append(fields, strconv.FormatBool(p.RequiredPullRequestReviews.RequireCodeOwnerReviews))
		bypassRules := []string{}
		if p.RequiredPullRequestReviews.BypassPullRequestAllowances != nil {
			for _, a := range p.RequiredPullRequestReviews.BypassPullRequestAllowances.Apps {
				if a.Slug != nil {
					bypassRules = append(bypassRules, fmt.Sprintf("app/%v", *a.Slug))
				}
			}
			for _, t := range p.RequiredPullRequestReviews.BypassPullRequestAllowances.Teams {
				if t.Slug != nil {
					bypassRules = append(bypassRules, fmt.Sprintf("team/%v", *t.Slug))
				}
			}
			for _, u := range p.RequiredPullRequestReviews.BypassPullRequestAllowances.Users {
				if u.Name != nil {
					bypassRules = append(bypassRules, fmt.Sprintf("user/%v", *u.Name))
				}
			}
		}
		fields = append(fields, strings.Join(bypassRules, ","))
	}

	return fields
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Missing required argument - path to file with newline-delimited org/repo strings")
	}
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()
	client := github.NewTokenClient(nil, os.Getenv("GITHUB_TOKEN"))

	fields := []string{
		"Organisation", "Repo", "Branch",
		"Branch exists?", "Branch protected?",
		"Any required status checks?", "Required status checks", "Strict status checks?",
		"Push restrictions?",
		"Admins included?",
		"Force pushes allowed?",
		"Deletions allowed?",
		"PR review required?", "CODEOWNERS required to review?", "Review bypasses allowed for?",
	}
	fmt.Println(strings.Join(fields, "\t"))

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), "/")
		org, repo := parts[0], parts[1]
		fmt.Println(strings.Join(report(ctx, client, org, repo, "main"), "\t"))
		fmt.Println(strings.Join(report(ctx, client, org, repo, "master"), "\t"))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
