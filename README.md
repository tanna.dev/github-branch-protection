# GitHub Branch protection reporter

A tool to list branch protection settings, [using the REST API](https://docs.github.com/en/rest/branches/branch-protection?apiVersion=2022-11-28#get-branch-protection), and produce a Tab Separated Value (TSV) formatted output, for use with tools like Google Sheets.

## Usage

First, install it, using:

```sh
go install gitlab.com/tanna.dev/github-branch-protection@HEAD
```

Then, create a file i.e. `repos.txt` i.e.:

```
jamietanna/dotfiles-arch
deepmap/oapi-codegen
```

Then run:

```sh
env GITHUB_TOKEN=... github-branch-protection repos.txt
```
